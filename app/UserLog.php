<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{

	protected $table = 'userlog';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['userid'];
}
